# withitems Integration Pack

StackStorm integration pack for sample dynamic checkbox and notification
1. `./actions/checkbox_inquiry.yaml` shows a sample workflow
1. `./rules/handle_inquiry.yaml` rule triggers notification workflow
1. `./actions/handle_inquiry.yaml` notification workflow

## Quick Start

Run the following commands to install this pack and the install  on your StackStorm host:

``` shell
st2 pack install checkbox_inquiry
st2 pack configure checkbox_inquiry
```

## Configuration

Copy the example configuration in [withitems.example.yaml](./checkbox_inquiry.example.yaml)
to `/opt/stackstorm/configs/checkbox_inquiry.yaml` and edit as required.

* `st2base` - url for base stackstorm api

### Configuration Example

The configuration below is an example of what a end-user config might look like.
``` yaml
st2base: https://localhost:443
```

## Actions

* `checkbox_inquiry.checkbox` sample workflow showing how to create a dynamic checkbox
* `handle_inquiry.yaml` notification workflow


## Rules

* `handle_inquiry.yaml` rule that triggers notification

